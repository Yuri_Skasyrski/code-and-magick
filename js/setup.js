'use strict';

(function () {
  // Учебный проект: одеть Надежду

  var setupOpen = document.body.querySelector('.setup-open');
  var setupClose = window.dialog.userDialog.querySelector('.setup-close');

  var openPopup = function () {
    window.dialog.userDialog.classList.remove('hidden');
    document.addEventListener('keydown', onPopupEscPress);
  };

  var closePopup = function () {
    window.dialog.userDialog.classList.add('hidden');
    window.dialog.userDialog.style.top = '100px'; //     магические -> !!!!!!!!!!!!!!!!!
    window.dialog.userDialog.style.left = '710px'; // -> цифры         !!!!!!!!!!!!!!!!!
    document.removeEventListener('keydown', onPopupEscPress);
  };


  setupOpen.addEventListener('click', function () {
    openPopup();
  });

  setupOpen.addEventListener('keydown', function (evt) {
    if (evt.key === window.util.ENTER_KEY) {
      openPopup();
    }
  });

  setupClose.addEventListener('click', function () {
    closePopup();
  });

  setupClose.addEventListener('keydown', function (evt) {
    if (evt.key === window.util.ENTER_KEY) {
      closePopup();
    }
  });

  var onPopupEscPress = function (evt) {
    if (evt.key === window.util.ESC_KEY) {
      closePopup();
    }
  };
})();
