'use strict';

(function () {
  // Перемещение диалога
  var dialogHandle = window.dialog.userDialog.querySelector('.setup-user');

  dialogHandle.addEventListener('mousedown', function (evt) {
    evt.preventDefault();

    // Запомним координаты точки, с которой мы начали перемещать диалог
    var startCoords = {
      x: evt.clientX,
      y: evt.clientY
    };

    // При каждом движении мыши нам нужно обновлять смещение относительно первоначальной точки,
    // чтобы диалог смещался на необходимую величину
    var onMouseMove = function (moveEvt) {
      moveEvt.preventDefault();

      var shift = {
        x: startCoords.x - moveEvt.clientX,
        y: startCoords.y - moveEvt.clientY
      };

      startCoords = {
        x: moveEvt.clientX,
        y: moveEvt.clientY
      };

      window.dialog.userDialog.style.top = (window.dialog.userDialog.offsetTop - shift.y) + 'px';
      window.dialog.userDialog.style.left = (window.dialog.userDialog.offsetLeft - shift.x) + 'px';
    };

    // При отпускании кнопки мыши нужно переставать слушать события движения мыши.
    var onMouseUp = function (upEvt) {
      upEvt.preventDefault();

      document.removeEventListener('mousemove', onMouseMove);
      document.removeEventListener('mouseup', onMouseUp);
    };

    // Добавим обработчики события передвижения мыши и отпускания кнопки мыши
    document.addEventListener('mousemove', onMouseMove);
    document.addEventListener('mouseup', onMouseUp);

    window.moving = {
      startCoords: startCoords
    };
  });
})();
