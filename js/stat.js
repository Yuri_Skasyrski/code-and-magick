'use strict';
// координаты левого верхнего угла бара
var START_X = 100; // координаты верхнего левого ->
var START_Y = 10; // -> угла гистограммы
var BAR_WIDTH = 420; // ширина гистограммы
var BAR_HEIGHT = 270; // высота гистограммы
var SHADOW_X = 10; // отступ ->
var SHADOW_Y = 10; // -> тени
var COLUMN_WIDTH = 40; // ширина колонки
var COLUMN_HEIGHT = 150; // высота наибольшей колонки = высоте гистограммы
var COLUMN_GAP = 50; // зазор между колонками
var NUMBER_OF_COLUMNS = 4; // количество колонок
var LEFT_MARGIN_OF_THE_HEADER = 20; // левый отступ заголовка
var LEFT_INDENT = 150; // левый отступ первой колонки
var LINE_LEVEL_1 = 30; // уровень первой строки заголовка
var LINE_LEVEL_2 = 50; // -> второй строки
var COLUMN_TOP_GAP = 100; // верхний зазор у колонок
var VALUE_GAP = 7; // зазор между результатом и колонкой

var names = ['Вы', 'Иван', 'Юлия', 'Стас']; // Игроки
// Их результаты через рандомную функцию
var creaeRandomValues = function (num) {
  var array = [];
  for (var j = 0; j < num; j++) {
    array.push(randomInteger(0, 10000));
  }
  return array;
};

var times = creaeRandomValues(NUMBER_OF_COLUMNS);

// найти максимальное число в массиве
var findMax = function (arr) {
  var max = 0;

  for (var i = 0; i < arr.length; ++i) {
    if (arr[i] > max) {
      max = arr[i];
    }
  }
  return max;
};
var maxRez = findMax(times);

// найти пропорциональную высоту колонки
var findHeight = function (index) {
  var rezult = times[index] / maxRez * COLUMN_HEIGHT;
  return Math.round(rezult);
};

// отрисовать прямоугольный бар
var renderCloud = function (ctx, x, y, color) {
  ctx.fillStyle = color;
  ctx.fillRect(x, y, BAR_WIDTH, BAR_HEIGHT);
};

// функция нахождения случайного числа в диапазоне
function randomInteger(min, max) {
  // случайное число от min до (max+1)
  var rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

// функция отрисовки гистограммы
window.renderStatistics = function (ctx) {
  renderCloud(ctx, START_X + SHADOW_X, START_Y + SHADOW_Y, 'rgba(0, 0, 0, 0.3)');
  renderCloud(ctx, START_X, START_Y, '#fff');

  ctx.fillStyle = '#000';
  ctx.font = '16px PT Mono';

  ctx.fillText('Ура вы победили!', START_X + LEFT_MARGIN_OF_THE_HEADER, START_Y + LINE_LEVEL_1);
  ctx.fillText('Список результатов:', START_X + LEFT_MARGIN_OF_THE_HEADER, START_Y + LINE_LEVEL_2);

  // цикл, отрисовывающий колонки
  var yourColor = 'rgba(255, 0, 0, 1)'; // цвет вашей колонки
  var columnColor; // цвет колонок других игроков
  for (var k = 0; k < NUMBER_OF_COLUMNS; k++) {
    columnColor = 'hsl(240, ' + randomInteger(0, 100) + '%, 50%)';
    var bottomPaddingForColumn = COLUMN_TOP_GAP + COLUMN_HEIGHT - findHeight(k);
    ctx.fillStyle = '#000';
    ctx.fillText(times[k], LEFT_INDENT + k * (COLUMN_GAP + COLUMN_WIDTH), bottomPaddingForColumn - VALUE_GAP);
    ctx.fillText(names[k], LEFT_INDENT + k * (COLUMN_GAP + COLUMN_WIDTH), BAR_HEIGHT);
    ctx.fillStyle = (names[k] !== 'Вы') ? columnColor : yourColor;
    ctx.fillRect(LEFT_INDENT + k * (COLUMN_GAP + COLUMN_WIDTH), bottomPaddingForColumn, COLUMN_WIDTH, findHeight(k));
  }
};
