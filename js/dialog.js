'use strict';

(function () {
  var userDialog = document.querySelector('.setup');

  // находим блок со списком похожих персонажей
  var similarListElement = userDialog.querySelector('.setup-similar-list');
  // находим содержимое шаблона для отрисовки волшебника
  var similarWizardTemplate = document.querySelector('#similar-wizard-template')
    .content
    .querySelector('.setup-similar-item');

  // вспомогательные функции
  // вызов случайного числа от 0 до number
  function random(number) {
    return Math.floor(Math.random() * (number + 1));
  }

  // функция отрисовки волшебника
  var renderWizard = function (wizard) {
    var wizardElement = similarWizardTemplate.cloneNode(true);

    var wizardName = wizard.names[random(wizard.names.length - 1)]; // случайное имя
    var wizardLastName = wizard.surnames[random(wizard.surnames.length - 1)]; // случайная фамилия
    wizardElement.querySelector('.setup-similar-label').textContent = wizardName + ' ' + wizardLastName;

    var coatColor = wizard.coatColors[random(wizard.coatColors.length)]; // случайный цвет мантии
    wizardElement.querySelector('.wizard-coat').style.fill = coatColor;

    var eyesColor = wizard.coatColors[random(wizard.eyeColors.length)]; // случайный цвет глаз
    wizardElement.querySelector('.wizard-eyes').style.fill = eyesColor;

    return wizardElement;
  };

  // создаем фрагмент со всеми волшебниками
  var fragment = document.createDocumentFragment();
  for (var i = 0; i < window.data.dataOfWizard.count; i++) {
    fragment.appendChild(renderWizard(window.data.dataOfWizard));
  }

  // добавляем этот фрагмент в блок с волшебниками
  similarListElement.appendChild(fragment);

  // удаляем класс hidden у блока с похожими волшебниками
  var setupSimilar = userDialog.querySelector('.setup-similar');
  setupSimilar.classList.remove('hidden');

  window.dialog = {
    userDialog: userDialog
  };
})();

